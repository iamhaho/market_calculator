# -*- coding: utf-8 -*-
"""
Created on Sat Mar 14 15:58:43 2020

@author: heeho

stock data for free for analysis
"""

import pandas as pd
import pandas_datareader.data as web
import datetime as dt
from datetime import datetime
import os
import sys

#sys.tracebacklimit = 0

def get_stock_data(ticker, start_date, end_date='', folder_name='stockdata',
                   save_data=True):
    '''
    A function to retrieve ticker information given
    start date and end date
    ticker: string - 'TICKER_NAME'  e.g. ticker = 'AAPL'
    For dates max possible import is up to range of 5 years.
    start_date = string dilimited by -. eg 'mm-dd-yyyy'
    end_date = string same as above. The default is '' which defines today.
    '''
    
    sd_s = start_date.split('-')
    s_year = int(sd_s[2])
    s_month = int(sd_s[0])
    s_day = int(sd_s[1])
    s_date = dt.datetime(s_year, s_month, s_day)
    if end_date == '':
        e_date = dt.datetime.today()
        end_date = '%i-%i-%i' % (e_date.month,e_date.day,e_date.year)
    else:
        ed_s = end_date.split('-')
        e_year = int(ed_s[2])
        e_month = int(ed_s[0])
        e_day = int(ed_s[1])
        e_date = dt.datetime(e_year, e_month, e_day)
        
    print(ticker)
    # this is sourcing from stooq website connected thru pandas_datareader
    df = web.get_data_yahoo(ticker, s_date, e_date)
    
    if save_data:
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        df.to_csv(folder_name+'/%s_%s_%s.csv' % (ticker, start_date, end_date))
        
    print(ticker, 'download complete')
    
    return df
    

