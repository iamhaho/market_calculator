# -*- coding: utf-8 -*-
"""
Created on Sat Mar 14 18:03:01 2020

@author: heeho
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates 

def plot_stock_chart(df,ticker='Your Stock'):
    fig, ax = plt.subplots(figsize=(16,9))
    
    ax.plot_date(pd.to_datetime(df.index),df['Adj Close'],
                 'b',label=ticker)
    plt.title(ticker)
    plt.ylabel('Price ($)')
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=3))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b\n%Y'))
    ax.legend()
    # high precision date on interactive plot
    ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d')
    
    plt.show()    

def plot_stock_chart_future(df1,df2,ticker='Your Stock'):
    fig, ax = plt.subplots(figsize=(16,9))
    
    ax.plot_date(pd.to_datetime(df1.index),df1['Adj Close'],
                 'b',label=ticker)
    ax.plot_date(pd.to_datetime(df2.index),df2['Adj Close'],
                 'g',label=ticker+' Future')
    plt.title(ticker)
    plt.ylabel('Price ($)')
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=3))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b\n%Y'))
    ax.legend()
    # high precision date on interactive plot
    ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d')
    
    plt.show()    

def interpret_buys(buy_price_points, buy_amount, peak_price, cash):
    ledger = []
    ledger_price = []       
    for j in range(len(buy_price_points)):
        if (type(buy_price_points[j]) == str):
            buy_price = float(buy_price_points[j])
        else:
            if buy_price_points[j] < 0.0:
                buy_price = (1.0+buy_price_points[j]*.01)*peak_price
            else:
                buy_price = buy_price_points[j]
        if (type(buy_amount[j]) == str):
            buy_in_cash = buy_price * float(buy_amount[j])
        else:
            if buy_amount[j] < 0.0:
                buy_in_cash = cash * -1.0*(buy_amount[j]*.01)
            else:
                buy_in_cash = buy_amount[j]
        shares = buy_in_cash/buy_price
        ledger.append(shares)
        ledger_price.append(buy_price)
    return ledger, ledger_price

