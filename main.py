# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import pandas as pd
import stock_data_loader as sdl
import utility as u
import functions as f
import datetime as dt
from IPython import get_ipython
get_ipython().run_line_magic('matplotlib', 'auto')

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates 


stock_ticker = 'AAPL' # e.g. 'AAPL', 'BTC-USD' or 'SPY' etc. 
                      # use Yahoo Finance tickers.
start_date = '06-01-2018'  # 'mm-dd-yyyy' American date format
plot_now = False  # plot downloaded data. turn off the plot with False
plot_future = True  # plot downloaded data with future.
percent_drop = 60  # estimated percent drop from the peak (cannot be negative)
future_peak = 80   # peak price of the future in percentages (%) of 
                   # the peak price in the data.
cash = 10000  # amount of cash to invest ($)
# for percent drops enter negative numbers. [-32.0, ... ]
# for share price enter put them in quotes ['185.32', ... ]
# any buy before today. If you haven't bought any set it = []
old_buy_price_points = ['325']  
# future buy price points.
new_buy_price_points = [-30., -40., -50.] 
# for number of shares put them in quotes. ['24', ... ]
# for a percent of bankroll enter negative numbers. [-24.0, ... ]
# for direct cash amount enter positive numbers. [165.01, ... ]
old_buy_amount       = ['20']  # any buy before today.
new_buy_amount       = [-33., -33., -34.]




buy_price_points = []
buy_price_points.extend(old_buy_price_points)
buy_price_points.extend(new_buy_price_points)
buy_amount = []
buy_amount.extend(old_buy_amount)
buy_amount.extend(new_buy_amount)

# data is in pandas dataframe format
dat = sdl.get_stock_data(stock_ticker,start_date=start_date, end_date='', 
                         folder_name='stockdata', save_data=False)
his = dat.High
lows = dat.Low
price_max = his.max()
date_price_max = str(his.idxmax().date())
price_min = lows.min()
date_price_min = str(lows.idxmax().date())
price_last = dat['Adj Close'][-1]
last_day = dat.index[-1]

x, y = f.quadratic_func(price_last, percent_drop, price_max, future_peak)
new_dates = []

# creating the future DataFrame
for i in range(len(x)):
    new_day = last_day + dt.timedelta(days=x[i])
    new_dates.append(new_day)
dat_future = pd.DataFrame({'Adj Close' : y}, index=new_dates)




ledger,ledger_price = u.interpret_buys(buy_price_points, buy_amount, 
                                       price_max, cash)
np_ledger = np.array(ledger)
np_ledger_price = np.array(ledger_price)
total_bankroll = np.sum(np_ledger*np_ledger_price)
buy_data = np.array([np_ledger, np_ledger_price])
print("Cash Now: %.2f" % cash)
print("Total Cash Invested: %.2f" % total_bankroll)
print("Final Cash : %.2f" % (np.sum(np_ledger)*price_max))


df1 = dat; df2 = dat_future; ticker=stock_ticker
fig, ax = plt.subplots(figsize=(16,9))

scatter_x = []
scatter_y = []

#for i in range(len())



ax.plot_date(pd.to_datetime(df1.index),df1['Adj Close'],
             'b',label=ticker)
ax.plot_date(pd.to_datetime(df2.index),df2['Adj Close'],
             'g',label=ticker+' Future')
plt.title(ticker)
plt.ylabel('Price ($)')
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=3))
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b\n%Y'))
ax.legend()
# high precision date on interactive plot
ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d')

plt.show() 


'''
if plot_now:
    u.plot_stock_chart(dat, stock_ticker)

if plot_future:
    u.plot_stock_chart_future(dat, dat_future, stock_ticker)
'''