# -*- coding: utf-8 -*-
"""
Created on Sun Mar 15 18:43:49 2020

@author: heeho
"""

import numpy as np

def quadratic_func(last_price, percent_drop, peak_price, future_factor=1.):
    '''
    INPUT
    last_price: float in $ (e.g. 294.12)
    peak_price: the peak price in the data base
    precent_drop: float in percentages (-precent_drop %); 
                  percent drop from the peak price
    quadractic function vertex form
    y = a*(x-h)**2 + k  -> the vertex is (h,k)
    prices = slope*(day-bottom_date)**2 + bottom_price
    This function assumes that it hits the bottom price in 100 days
    and then peaks.
    '''

    bottom_price = peak_price - (peak_price * percent_drop*0.01)
    if bottom_price > last_price:
        bottom_price = last_price
        print('Warning. percent_drop * price_max is greater than the last'
              ' price')
        print('======> Using the last price as the bottom price.')

    k = bottom_price
    # represents h days later when it bottoms out. scaled by the price
    # 600 should be determined by the current length of the graph.
    h = 600.*(last_price-bottom_price)/last_price
    # a is the slope of the curvature
    if h < 0.01:
        a = 0.1
    else:
        a = (last_price - bottom_price) / h**2.
    # peak_price x future_factor is the future peak price
    x_max = int(h + np.sqrt((peak_price*future_factor*0.01 - k)/a))
    x = np.linspace(0,x_max,x_max+1)
    y = a*(x-h)**2. + k
    
    return x, y

def quartic_func():
    pass